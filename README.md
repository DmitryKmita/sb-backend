Backend Test
=============================

# Build for production:
1. Package the source using maven:
``` mvn package```
2. Run the target/backend-test-1.0*.jar file:
``` java -jar ./target/backend-test-1.0-SNAPSHOT-jar-with-dependencies.jar ```

# Build for dev:
1. Run Application from app.Main class:

# Notes:

* Used TDD
* Used lombock for autogeneration + test coverage (testing setters is inefficient)
* Used Log4J for simple logging