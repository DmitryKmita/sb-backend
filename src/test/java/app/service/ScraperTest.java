package app.service;

import app.model.ProductList;
import org.junit.Assert;
import org.junit.Test;

public class ScraperTest {

    @Test
    public void test_WhenIGetHTMLIScrapeProductBlocks()
    {
        Scraper scraper = new Scraper();
        ProductList productList = scraper.scrapeUrl("https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html");
        Assert.assertNotNull(productList);
    }
}
