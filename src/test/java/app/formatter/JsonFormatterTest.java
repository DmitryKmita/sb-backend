package app.formatter;

import app.model.Product;
import app.model.ProductList;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

public class JsonFormatterTest {
    private String expectedJson = "{\"results\":[{\"title\":\"Title\",\"kcal_per_100g\":100,\"unit_price\":12.20,\"description\":\"Description\"}],\"total\":12.20}";

    @Test
    public void test_WhenIPassProductListIShouldGetValidJSONBack()
    {
        JsonFormatter jsonFormatter = new JsonFormatter();

        Product product = new Product();
        product.setTitle("Title");
        product.setDescription("Description");
        product.setKcalPer100Grams(100);
        product.setUnitPrice(new BigDecimal("12.20"));

        ArrayList<Product> productArrayList = new ArrayList<>();
        productArrayList.add(product);

        ProductList productList = new ProductList(productArrayList);

        Assert.assertEquals(expectedJson, jsonFormatter.productListToJson(productList));
    }
}
