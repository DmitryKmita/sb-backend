package app.helper;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class PriceParserTest {

    @Test
    public void test_WhenIPassSimpleStringIShouldGetSameBigInteger()
    {
        BigDecimal expectedDecimal = new BigDecimal("100.00");
        BigDecimal actualDecimal = PriceParser.parsePrice("100.00");
        Assert.assertTrue(expectedDecimal.equals(actualDecimal));
    }

    @Test
    public void test_WhenIPassStringWithCurrencyIShouldGetSameBigInteger()
    {
        BigDecimal expectedDecimal = new BigDecimal("100.00");
        BigDecimal actualDecimal = PriceParser.parsePrice("£100.00");
        Assert.assertTrue(expectedDecimal.equals(actualDecimal));
    }

    @Test
    public void test_WhenIPassStringWithCurrencyAndUnitsIShouldGetSameBigInteger()
    {
        BigDecimal expectedDecimal = new BigDecimal("100.00");
        BigDecimal actualDecimal = PriceParser.parsePrice("£100.00/units");
        Assert.assertTrue(expectedDecimal.equals(actualDecimal));
    }
}
