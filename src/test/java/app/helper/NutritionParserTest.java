package app.helper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

public class NutritionParserTest {

    @Test
    public void test_WhenIHavePageWithSimpleNutritionInfoIGetRightNumberOfKCal() {
        String html = "<table class=\"nutritionTable\">\n" +
                            "<thead>\n" +
                                "<tr>\n" +
                                    "<th scope=\"col\"></th><th scope=\"col\">(as sold) per 100g</th><th scope=\"col\"> % adult RI per 100g</th><th scope=\"col\">adult RI</th>\n" +
                                "</tr>\n" +
                            "</thead>\n" +
                            "<tbody>\n" +
                                "<tr>\n" +
                                    "<th scope=\"row\" class=\"rowHeader\">Energy kJ</th><td>222</td><td>-</td><td>8400</td>\n" +
                                "</tr>\n" +
                                "<tr>\n" +
                                    "<th scope=\"row\" class=\"rowHeader\">Energy kcal</th><td>100</td><td>3%</td><td>2000</td>\n" +
                                "</tr>\n" +
                                "<tr>\n" +
                                    "<th scope=\"row\" class=\"rowHeader\">Fat </th><td>&lt;0.5g</td><td>&lt;1%</td><td>70g</td>\n" +
                                "</tr>\n" +
                            "</tbody>\n" +
                        "</table>";

        Document nutritionalDocument = Jsoup.parse(html);
        Element nutritionalElement = nutritionalDocument.selectFirst(".nutritionTable");
        Integer actual = NutritionParser.getEnergyKCal(nutritionalElement);

        Assert.assertEquals(Integer.valueOf(100), actual);
    }

    @Test
    public void test_WhenIHavePageWithComplexNutritionInfoIGetRightNumberOfKCal() {
        String html = "<table class=\"nutritionTable\">\n" +
                "<thead>\n" +
                "<tr class=\"tableTitleRow\">\n" +
                "<th scope=\"col\">Typical Values</th><th scope=\"col\">Per 100g&nbsp;</th><th scope=\"col\">% based on RI for Average Adult</th>\n" +
                "</tr>\n" +
                "</thead>\n" +
                "<tbody>" +
                "<tr class=\"tableRow1\">\n" +
                "<th scope=\"row\" class=\"rowHeader\" rowspan=\"2\">Energy</th><td class=\"tableRow1\">220kJ</td><td class=\"tableRow1\">-</td>\n" +
                "</tr>\n" +
                "<tr class=\"tableRow0\">\n" +
                "<td class=\"tableRow0\">100kcal</td><td class=\"tableRow0\">2%</td>\n" +
                "</tr>\n" +
                "<tr class=\"tableRow1\">\n" +
                "<th scope=\"row\" class=\"rowHeader\">Fat</th><td class=\"tableRow1\">&lt;0.5g</td><td class=\"tableRow1\">-</td>\n" +
                "</tr>\n" +
                "<tr class=\"tableRow0\">\n" +
                "<th scope=\"row\" class=\"rowHeader\">Saturates</th><td class=\"tableRow0\">&lt;0.1g</td><td class=\"tableRow0\">-</td>\n" +
                "</tr>\n" +
                "</tbody>" +
                "</table>";

        Document nutritionalDocument = Jsoup.parse(html);
        Element nutritionalElement = nutritionalDocument.selectFirst(".nutritionTable");
        Integer actual = NutritionParser.getEnergyKCal(nutritionalElement);

        Assert.assertEquals(Integer.valueOf(100), actual);
    }
}
