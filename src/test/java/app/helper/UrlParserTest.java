package app.helper;


import org.junit.Assert;
import org.junit.Test;

public class UrlParserTest {

    private static String ACTUAL_URL = "http://www.domain.com/path/to/url/index.html";
    private static String EXPECTED_PATH = "http://www.domain.com/path/to/url";

    @Test
    public void test_WhenIPassFullUrlIShouldGetFullPath() {
        Assert.assertEquals(EXPECTED_PATH, UrlParser.getMainPath(ACTUAL_URL));
    }
}
