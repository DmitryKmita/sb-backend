package app.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ProductListTest {

    @Test
    public void test_WhenICreateProductListItShouldCalculateTotals() {
        Product product = new Product();
        product.setUnitPrice(new BigDecimal("12.20"));

        Product product2 = new Product();
        product2.setUnitPrice(new BigDecimal("18.20"));

        ArrayList<Product> productArrayList = new ArrayList<>();
        productArrayList.add(product);
        productArrayList.add(product2);

        ProductList productList = new ProductList(productArrayList);

        Assert.assertTrue(productList.getTotal().equals(new BigDecimal("30.40")));
    }
}
