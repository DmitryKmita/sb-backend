package app.formatter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import app.model.ProductList;

public class JsonFormatter {
    public String productListToJson(ProductList productList) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().disableHtmlEscaping().create();
        return gson.toJson(productList);
    }
}
