package app;

import app.formatter.JsonFormatter;
import lombok.extern.log4j.Log4j;
import app.model.ProductList;
import app.service.Scraper;

@Log4j
public class Main {

    private static String baseUrl = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";

    public static void main(String[] args) {
        Scraper scraper = new Scraper();
        ProductList productList = scraper.scrapeUrl(baseUrl);
        JsonFormatter formatter = new JsonFormatter();
        System.out.println(formatter.productListToJson(productList));
    }
}
