package app.service;

import app.helper.NutritionParser;
import app.helper.PriceParser;
import app.helper.UrlParser;
import lombok.extern.log4j.Log4j;
import app.model.Product;
import app.model.ProductList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

@Log4j
public class Scraper {

    public ProductList scrapeUrl(String url) {
        String mainUrlPath = UrlParser.getMainPath(url);
        ArrayList<Product> productList = new ArrayList<>();
        try {
            Document listPageDocument = Jsoup.connect(url).get();
            Elements products = listPageDocument.select(".gridItem .product");
            for (Element productElement : products) {
                Product product = this.hydrateProduct(productElement);
                try {
                    this.processSinglePage(mainUrlPath + "/" + product.getProductUrl(), product);
                    productList.add(product);
                } catch (IOException exception) {
                    log.warn("[!] Skipping product: could not parse product single page.");
                }
            }
        } catch (IOException exception) {
            log.warn("[!] Skipping products: could not parse product list page.");
        }

        return new ProductList(productList);
    }

    private void processSinglePage(String url, Product product) throws IOException {
        Document singlePageDocument = Jsoup.connect(url).get();
        Elements descriptionLines = singlePageDocument.select(".productText p");
        String description = "";
        for (Element descriptionLine: descriptionLines) {
            if (!descriptionLine.text().equals("")) {
                description = descriptionLine.text();
                break;
            }
        }
        product.setKcalPer100Grams(NutritionParser.getEnergyKCal(singlePageDocument.selectFirst(".nutritionTable")));
        product.setDescription(description);
    }

    private Product hydrateProduct(Element productElement) {
        Product product = new Product();

        Element titleElement = productElement.selectFirst("h3 a");

        String title = titleElement.text();
        String productUrl = titleElement.attr("href");

        BigDecimal pricePerUnit = PriceParser.parsePrice(productElement.selectFirst(".pricePerUnit").text());
        product.setTitle(title);
        product.setProductUrl(productUrl);

        product.setUnitPrice(pricePerUnit);

        return product;
    }
}
