package app.helper;

import java.math.BigDecimal;

public class PriceParser {
    public static BigDecimal parsePrice(String stringPrice) {
        stringPrice = stringPrice.split("/")[0];
        stringPrice = stringPrice.replace('£', ' ').trim();
        BigDecimal price = new BigDecimal(stringPrice);
        price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
        return price;
    }
}
