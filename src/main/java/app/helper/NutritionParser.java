package app.helper;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class NutritionParser {
    private final static String DUAL_LINE_ENERGY_HEADER = "Energy";
    private final static String SINGLE_LINE_ENERGY_HEADER = "Energy kcal";

    public static Integer getEnergyKCal(Element nutritionalElement) {
        String kCal = "";
        if (nutritionalElement != null) {
            Elements rows = nutritionalElement.select("tr");
            for (Element row : rows) {
                Element header = row.selectFirst(".rowHeader");
                if (header != null) {
                    if (header.text().equals(DUAL_LINE_ENERGY_HEADER)) {
                        Element nextRow = row.nextElementSibling();
                        kCal = nextRow.selectFirst("td").text();
                    }
                    if (header.text().equals(SINGLE_LINE_ENERGY_HEADER)) {
                        kCal = row.selectFirst("td").text();
                    }
                }
            }
            kCal = kCal.replace("kcal", "");
        }
        return kCal.equals("") ? null : Integer.valueOf(kCal);
    }
}
