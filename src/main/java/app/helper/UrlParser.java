package app.helper;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class UrlParser {
    public static String getMainPath(String url) {
        List<String> parsedUrl = new LinkedList<>(Arrays.asList(url.split("/")));
        parsedUrl.remove(parsedUrl.size()-1);
        return parsedUrl.stream().collect(Collectors.joining("/"));
    }
}
