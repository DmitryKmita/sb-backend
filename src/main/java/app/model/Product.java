package app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class Product {
    @Expose
    String title;

    @Expose
    @SerializedName("kcal_per_100g")
    Integer kcalPer100Grams;

    @Expose
    @SerializedName("unit_price")
    BigDecimal unitPrice;

    @Expose
    String description;

    String productUrl;
}
