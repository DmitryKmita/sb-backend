package app.model;

import com.google.gson.annotations.Expose;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.ArrayList;

@Getter
public class ProductList {

    @Expose
    private ArrayList<Product> results;

    @Expose
    private BigDecimal total;

    public ProductList(ArrayList<Product> productList) {
        this.results = productList;
        BigDecimal total = new BigDecimal("0.00");

        for(Product product: productList) {
            total = total.add(product.getUnitPrice());
        }
        this.total = total;
    }
}
